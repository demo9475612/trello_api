const {apiKey,token} = require('./keyToken');
// function delayedCheck(checkItem, index,cardId){
    
//       if (index === 0) {
//         resolve(UncheckItemsAll(checkItem.id, cardId));
//       } else {
//         setTimeout(() => {
//           resolve(UncheckItemsAll(checkItem.id, cardId));
//         }, 1000 * index); 
//       }
//     });
// }
function UncheckItemsAll(itemsId,cardId){
    return fetch(`https://api.trello.com/1/cards/${cardId}/checkItem/${itemsId}?state=incomplete&key=${apiKey}&token=${token}`, {
        method: 'PUT'
        })
        .then(response => {
            return response.json();
        })
}

function getCheckItemsData(checkListId) {
    return fetch(
      `https://api.trello.com/1/checklists/${checkListId[0]}/checkItems?key=${apiKey}&token=${token}`,
      {
        method: "GET",
      }
    )
      .then((response) => {
        return response.json();
      })
      .then((checkitemdata) => {
        const cardId = checkListId[1];
        const promiseResolved =Promise.resolve();
        checkitemdata.forEach((checkItemsData,index) =>{
          promiseResolved.then(()=>{
            setTimeout(() =>{
              UncheckItemsAll(checkItemsData.id,cardId);
              console.log(`Unchecked ${checkItemsData.id}`)
            },index*1000)
          });
        });
        return promiseResolved;
      })
      .catch((error)=>console.log(error));
  }
  function getAllCheckListId(boardID){
    return fetch(
        `https://api.trello.com/1/boards/${boardID}/checklists?key=${apiKey}&token=${token}`,
        {
          method: "GET",
        }
      ).then((response) => {
        return response.json();
      })
      .catch((error)=>console.log(error));
}
function updateAllCheckitmesInComplete(boardId){
    return  getAllCheckListId(boardId)
        .then((AllcheckLists) => {
            //console.log(checkLists);
            let checkListIds = AllcheckLists.reduce((listOfAllIdAndCardId, checkList) => {
                let idAndCardId = [];
                idAndCardId.push(checkList.id);
                idAndCardId.push(checkList.idCard);
                listOfAllIdAndCardId.push(idAndCardId);
                return listOfAllIdAndCardId;
            }, []);
            //console.log(checkListIds);
            return checkListIds;
        })
        .then((checkListIds) => {
            return Promise.all(checkListIds.map((idAndCardId) => getCheckItemsData(idAndCardId)));
        })
        .catch((error)=>console.log(error));
}
module.exports = updateAllCheckitmesInComplete;