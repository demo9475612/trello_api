// /*Delete all the lists created in Step 6 simultaneously*/

const getLists = require("./problem3");
const { apiKey, token } = require('./keyToken');

function deleteList(listId) {
    const url = `https://api.trello.com/1/lists/${listId}/closed?value=true&key=${apiKey}&token=${token}`;
    return fetch(url, {
        method: 'PUT'
    })
    .then(response => {
        if (!response.ok) {
            throw new Error('Failed to delete list');
        }
        return response.json();
    });
}

function deleteAllLists(boardId){
    return getLists(boardId)
    .then((listIds)=>{
        //console.log(listIds);
        const deletePromises = listIds.map(listId=>deleteList(listId.id));
        return Promise.all(deletePromises);
    })
    .then((deletedListItem)=>{
        console.log("All list are deleted successfully");
        return deletedListItem;
    })
    .catch((error)=>{
        console.error(error);
    })
}
module.exports = deleteAllLists;