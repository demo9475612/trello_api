// /*Create a function getCards which takes a listId as argument and returns a promise which resolves with cards data*/
const { apiKey, token } = require('./keyToken');
function fetchDataFromUrl(url){
    const fetchValue= fetch(url,{
        method:'GET',
        headers:{
            Accept : 'application/json',
        }
    })
    .then((response)=>{
        //console.log(response);
        const statusCode= response.status;
        const statusMessage= response.statusText;
        console.log(`Fetch Response ${statusCode} ${statusMessage}`);
        //console.log(response.json());
        return response.json();
        

    })
    .catch((error)=>{
        console.error(error);
    })
    return fetchValue;
}


function getCards(listId) {
    const url = `https://api.trello.com/1/lists/${listId}/cards?key=${apiKey}&token=${token}`;
    return fetchDataFromUrl(url); 
}

module.exports = getCards;
