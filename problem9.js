// /*Update all checkitems in a checklist to completed status simultaneously */

 const { apiKey, token } = require('./keyToken');

function checkItemsAll(itemsId,cardId){
    return fetch(`https://api.trello.com/1/cards/${cardId}/checkItem/${itemsId}?state=complete&key=${apiKey}&token=${token}`, {
        method: 'PUT'
        })
        .then(response => {
            return response.json();
        })
        .catch((error)=>console.log(error));
}


function getAllCheckListId(boardID){
    return fetch(
        `https://api.trello.com/1/boards/${boardID}/checklists?key=${apiKey}&token=${token}`,
        {
          method: "GET",
        }
      ).then((response) => {
        return response.json();
      })
      .catch((error)=>console.log(error));
}

function getCheckItemsData(checkListId) {
    return fetch(
      `https://api.trello.com/1/checklists/${checkListId[0]}/checkItems?key=${apiKey}&token=${token}`,
      {
        method: "GET",
      }
    )
      .then((response) => {
        return response.json();
      })
      .then((checkitemdata) => {
        return Promise.all(
          checkitemdata.map((checkItem) => checkItemsAll(checkItem.id, checkListId[1]))
        );
      })
      .catch((error)=>console.log(error));
  }
  
  function updateAllCheckItemsToCompleted(boardID) {
  
      return  getAllCheckListId(boardID)
          .then((AllcheckLists) => {
              //console.log(checkLists);
              let checkListIds = AllcheckLists.reduce((listOfAllIdAndCardId, checkList) => {
                  let idAndCardId = [];
                  idAndCardId.push(checkList.id);
                  idAndCardId.push(checkList.idCard);
                  listOfAllIdAndCardId.push(idAndCardId);
                  return listOfAllIdAndCardId;
              }, []);
              //console.log(checkListIds);
              return checkListIds;
          })
          .then((checkListIds) => {
              return Promise.all(checkListIds.map((idAndCardId) => getCheckItemsData(idAndCardId)));
          })
          .catch((error)=>console.log(error));
  }
  
  module.exports = updateAllCheckItemsToCompleted;
  