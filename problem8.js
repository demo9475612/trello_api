/* Delete all the lists created in Step 6 sequentially i.e. List 1 should be deleted -> then List 2 should be deleted etc.*/
const getLists = require("./problem3");
const { apiKey, token } = require('./keyToken');

function deleteList(listId) {
    const url = `https://api.trello.com/1/lists/${listId}/closed?value=true&key=${apiKey}&token=${token}`;
    return fetch(url, {
        method: 'PUT'
    })
    .then(response => {
        if (!response.ok) {
            throw new Error('Failed to delete list');
        }
        return response.json();
    });
}

function deleteAllListsSequentially(boardId){
    return getLists(boardId)
    .then((listIds)=>{
        let deletePromise = Promise.resolve();
        for (const listId of listIds) {
            deletePromise = deletePromise.then(() => deleteList(listId.id));
        }
        return deletePromise;
    })
    .then((resolveData) => {
        return resolveData;
    })
    .catch((error)=>{
        console.error(error);
    });
}

module.exports = deleteAllListsSequentially;
