/*Create a new board, create 3 lists simultaneously, and a card in each list simultaneously */

const { apiKey, token } = require('./keyToken');
const createBoard = require('./problem2');


function createList(boardId, listName) {
    const url = `https://api.trello.com/1/lists?name=${listName}&idBoard=${boardId}&key=${apiKey}&token=${token}`;
    return fetch(url, {
        method: 'POST'
    })
    .then(response => {
        //console.log(`Response: ${response.status} ${response.statusText}`);
        return response.json();
    })
    .catch(err => console.error(err));
}

function createCard(listId, cardName) {
    const url = `https://api.trello.com/1/cards/?name=${cardName}&idList=${listId}&key=${apiKey}&token=${token}`;
    return fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => {
        if (!response.ok) {
            throw new Error('Failed to create card');
        }
        return response.json();
    });
}

function createBoardWithListsAndCards(boardName, listNames, cardNames) {
    return createBoard(boardName)
        .then(board => {
            const boardId = board.id;
            console.log(`board created successfully boardId- ${boardId}`);
            const listCreatedPromises = listNames.map(listName => createList(boardId, listName));
            //console.log(listCreatedPromises);
            return Promise.all(listCreatedPromises);
        })
        .then(lists => {
            //console.log(lists);
            console.log(`Lists created successfully`)
            const cardCreatePromises = lists.map((list, index) => createCard(list.id, cardNames[index]));
            //console.log(cardCreatePromises);
            return Promise.all(cardCreatePromises);
        })
        .then((cards)=>{
            console.log(`Cards created successfully`);
            return cards;
        })
        .catch(error => {
            console.error('Error creating board with lists and cards:', error);
        });
}

module.exports = createBoardWithListsAndCards;