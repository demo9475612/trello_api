/*Create a function getAllCards which takes a boardId as argument and which uses getCards function to fetch cards of all the lists.
 Do note that the cards should be fetched simultaneously from all the lists. */

const getLists = require('./problem3');
const getCards = require('./problem4');

function getCardFromListIds(listId){
   //console.log(listIds);
   const listIdsArray = listIds.map(listId => getCards(listId.id));
   //console.log(listIdArray);
   return Promise.all(listIdsArray);
}
function getListOfCards(listOfListsOfCards){
    //console.log(listOfListsOfCards);
    return listOfListsOfCards.reduce((allCards, cards) =>{
         allCards.concat(cards)
        }, []);
}
function getAllCards(boardId) {
    return getLists(boardId)
        .then(listIds => {
            return getCardFromListIds(listIds);
        })
        .then(listOfListsOfCards => {
            return getCardFromListIds(listOfListsOfCards);
        })
        .catch(error => {
            console.error('Error fetching all cards:', error);
            throw error;
        });
}
module.exports = getAllCards;
