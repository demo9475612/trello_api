/*Update all checkitems in a checklist to incomplete status sequentially i.e. 
Item 1 should be updated -> then wait for 1 second -> then Item 2 should be updated etc. */

const updateAllCheckitmesInComplete = require("../problem10");

const boardId ='uQkal0dJ' ;
try{
    updateAllCheckitmesInComplete(boardId)
    .then(()=>{
        console.log("All lists are marked Incompleted");
    })
    .catch((error)=>{
        console.log(error.message);
    });
    
}catch(error){
    console.error(error);
}