const createBoardWithListsAndCards = require("../problem6");

const boardName = "NewBoard";
const listNames = ["List 1", "List 2", "List 3"];
const cardNames = ["Card 1", "Card 2", "Card 3"];
try{
    createBoardWithListsAndCards(boardName, listNames, cardNames)
    .then(cards => {
        console.log('Board with lists and cards created successfully');
    })
    .catch(error => {
        console.error('Error:', error);
    });
}catch(error){
    console.error(error);
}