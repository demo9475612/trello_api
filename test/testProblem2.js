const createBoard = require("../problem2.js");

const boardName='boardOne';
try{
    createBoard(boardName)
    .then((resolveData)=>{
        console.log(resolveData);
    })
    .catch((error)=>{
        console.log(error.message);
    });
}catch(error){
    console.error(error);
}