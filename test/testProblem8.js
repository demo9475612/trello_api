const deleteAllListsSequentially = require("../problem8");

const boardId ='uQkal0dJ' ;
try{
    deleteAllListsSequentially(boardId)
    .then((resolveData)=>{
        if(resolveData==undefined){
        console.log("All lists are deleted successfully");
        }
    })
    .catch((error)=>{
        console.log(error.message);
    });
    
}catch(error){
    console.error(error);
}
