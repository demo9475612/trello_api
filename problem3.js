/*Create a function getLists which takes a boardId as argument and returns a promise which resolved with lists data*/
const {apiKey,token} = require('./keyToken');

function fetchDataFromUrl(url) {
  const fetchValue= fetch(url, {
    method: "GET",
    headers: {
      Accept: "application/json",
    }
  })
  .then((fetchResponse) => {
    const statusCode = fetchResponse.status;
    const statusMessage = fetchResponse.statusText;    
    console.log(`Fetch Response: ${statusCode} ${statusMessage}`);
    //console.log(fetchResponse);
    return fetchResponse.json();
  })
  .catch((error) => {
    console.error(error);
  });
  //console.log(fetchValue);
  return fetchValue;
}
function getLists(boardId){
    const url = `https://api.trello.com/1/boards/${boardId}/lists?key=${apiKey}&token=${token}`;
    return fetchDataFromUrl(url);
}
module.exports=getLists;