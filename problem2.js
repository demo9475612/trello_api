/*Create a function createBoard which takes the boardName as argument and returns a promise which resolves with newly created board data*/

const {apiKey,token} = require('./keyToken');
const permissionLevel = 'public';
const desc = 'Board Created';
function fetchDataFromUrl(url) {
    const fetchValue= fetch(url,{
        method : 'POST'
    })
    .then((fetchResponse) => {
      const statusCode = fetchResponse.status;
      const statusMessage = fetchResponse.statusText;    
      console.log(`Fetch Response: ${statusCode} ${statusMessage}`);
      //console.log(fetchResponse);
      return fetchResponse.json();
    })
    .catch((error) => {
      console.error(error);
    });
    //console.log(fetchValue);
    return fetchValue;
  }
function createBoard(boardName){
    const url = `https://api.trello.com/1/boards/?name=${boardName}&key=${apiKey}&token=${token}&prefs_permissionLevel=${permissionLevel}&desc=${desc}`;
    return fetchDataFromUrl(url);
}
module.exports = createBoard;
